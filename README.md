# aws-cert

A command line tool to create Amazon issued certificates and add them to load-balancers.

## Install

Clone the projet wherever you have a version of Ruby and where you can install gems.
Use Bundler to install the dependencies : `$ bundle install`

## Configuration

You can create a `.env` file next to the script with configuration variables. It will be loaded by the script. Example :

```
$ cat .env

ELB_ARN="arn:aws:elasticloadbalancing:eu-west-3:123456789109:listener/app/MyELB/"
ELB_LISTENER_ARN="arn:aws:elasticloadbalancing:eu-west-3:123456789109:listener/app/MyELB/5b7f89aedb97fe05/8776e6f6d330148f"
AWS_REGION=eu-west-3
AWS_SECRET_ACCESS_KEY=
AWS_ACCESS_KEY_ID=
```

If you already use a global AWS configuration in `~/.aws/config` and `~/.aws/credentials` they will be loaded.
If you have profiles, you can add the profile name in the `.env` file : `AWS_PROFILE=my-profile`.

## Usage

Sub-commands usually require an argument. You can use the domain name or the ARN.

### List certificates

```
$ ./aws-cert list

Domain name: *.example.com
  ARN: arn:aws:acm:eu-west-3:457017632624:certificate/9ae462d1-7042-4ee5-ab37-2b21baf9bdd9
Domain name: foo.bar.com
  ARN: arn:aws:acm:eu-west-3:457017632624:certificate/f010c92e-f1f3-4ce2-96b1-9c64a1b70a89
```

### Create a certificate

```
$ ./aws-cert create my.domain.com

Domain name:
  ARN: arn:aws:acm:eu-west-3:457017632624:certificate/32d8ee16-0a3b-489a-8ebe-f3e036c0c2f4
  Status: PENDING_VALIDATION
  Created at: 2019-10-22 10:39:39 +0200
  Not before:
  Not after:
  Issuer: Amazon
  Renewable eligibility: INELIGIBLE
  Validation option: _unavailable_
```

At this moment, the details of the certificates are unavailable. You must ask for the status after a few seconds.

### Certificate status

```
$ ./aws-cert status my.domain.com

Domain name: my.domain.com
  ARN: arn:aws:acm:eu-west-3:457017632624:certificate/32d8ee16-0a3b-489a-8ebe-f3e036c0c2f4
  Status: PENDING_VALIDATION
  Created at: 2019-10-22 10:39:39 +0200
  Not before:
  Not after:
  Issuer: Amazon
  Renewable eligibility: INELIGIBLE
  Validation option: DNS
    Status: PENDING_VALIDATION
    Record name: _d5db3c0777493789dad41b72b45fc568.my.domain.com.
    Record type: CNAME
    Record value: _e889b6f815e2f5f70ec50b88db2e55ff.kirrbxfjtw.acm-validations.aws.

```

You need to update your domain zone in the DNS with a `CNAME` record to validate your ownership of the domain.

After a few minutes/hours, AWS will validate the domain.

```
$ ./aws-cert status my.domain.com

Domain name: my.domain.com
  ARN: arn:aws:acm:eu-west-3:457017632624:certificate/32d8ee16-0a3b-489a-8ebe-f3e036c0c2f4
  Status: ISSUED
  Created at: 2019-10-22 14:34:59 +0200
  Not before: 2019-10-22 02:00:00 +0200
  Not after: 2020-11-22 13:00:00 +0100
  Issuer: Amazon
  Renewable eligibility: INELIGIBLE
  Validation option: DNS
    Status: SUCCESS
    Record name: _d5db3c0777493789dad41b72b45fc568.my.domain.com.
    Record type: CNAME
    Record value: _e889b6f815e2f5f70ec50b88db2e55ff.kirrbxfjtw.acm-validations.aws.
```

If the status is `ISSUED`, you can proceed to the ELB.

### Enable the certificate in the ELB

```
$ ./aws-cert enable my.domain.com

Certificate has been added to load-balancer, change DNS configuration :
  Record name: my.domain.com.
  Record type: CNAME
  Record value: MyELB-123456789.eu-west-3.elb.amazonaws.com.
```

You need to update your domain zone in the DNS with a `CNAME` record to point to the ELB domain.
